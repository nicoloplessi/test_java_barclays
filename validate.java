package xsdvalidator;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.MalformedURLException;
import java.util.Scanner;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

public class validate {

    private final static String PROGRAM_NAME = "xsdv";
    private final static String VERSION = "1.1";
    private final static int VALIDATION_FAIL = 1;
    private final static int ERROR_READING_SCHEMA = 2;
    private final static int ERROR_READING_XML = 3;

    private static String mXSDFileName;
    private static String mXMLFileName;
    private static Exception x;
    public static void main(String[] args) throws IOException, ParserConfigurationException, SAXException {
        int y=5;  
        do{
         y=valida(args);   
        }while(y!=1);
        
    }
    
    public static int valida(String[] args) throws IOException, ParserConfigurationException
    {
        parseArgs(args);
        SchemaFactory factory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");

        File XMLFile = new File(mXMLFileName);

        try {
            URL url = testURL(mXSDFileName);

            try {
                Schema schema = factory.newSchema(url);
                Validator validator = schema.newValidator();

                Source source = new StreamSource(XMLFile);
               
                try {
                    x=null;
                    validator.validate(source);
                    
                    System.out.println(mXMLFileName + " validates");
                    return 1;
                } catch (SAXParseException ex) {
                    correggi(ex);
                    x=ex;
                    return 0;
                }
                
                catch (SAXException ex) {
                    File f = new File(XMLFile.getName() + ".ko");

                    FileWriter fw = new FileWriter(f, true);
                    BufferedWriter bw = new BufferedWriter(fw);
                    if (!f.exists()) {
                        f.createNewFile();
                    }
                    System.out.println(mXMLFileName + " fails to validate because: \n");
                    System.out.println(ex.getMessage());
                    System.out.println();
                    bw.write(mXMLFileName + " fails to validate because: \n");
                    bw.write(ex.getMessage());
                    bw.close();
                    fw.close();

                    System.exit(VALIDATION_FAIL);

                } catch (IOException io) {
                    File f = new File(XMLFile.getName() + ".ko");

                    FileWriter fw = new FileWriter(f, true);
                    BufferedWriter bw = new BufferedWriter(fw);
                    if (!f.exists()) {
                        f.createNewFile();
                    }
                    System.err.println("Error reading XML source: " + mXMLFileName);
                    System.err.println(io.getMessage());
                    bw.write("Error reading XML source: " + mXMLFileName);
                    bw.write(io.getMessage());
                    bw.close();
                    fw.close();
                    System.exit(ERROR_READING_XML);
                }

            } catch (SAXException sch) {
                File f = new File(XMLFile.getName() + ".ko");

                FileWriter fw = new FileWriter(f, true);
                BufferedWriter bw = new BufferedWriter(fw);
                if (!f.exists()) {
                    f.createNewFile();
                }
                System.err.println("Error reading XML Schema: " + mXSDFileName);
                System.err.println(sch.getMessage());
                System.exit(ERROR_READING_SCHEMA);
            }
        } catch (MalformedURLException e) {
            File f = new File(XMLFile.getName() + ".ko");

            FileWriter fw = new FileWriter(f, true);
            BufferedWriter bw = new BufferedWriter(fw);
            if (!f.exists()) {
                f.createNewFile();
            }
            System.err.println("Error URL syntax: " + mXSDFileName);
            System.err.println(e.getMessage());
            System.exit(ERROR_READING_SCHEMA);
        }
            return 2;
    }
    
    public static void correggi(SAXParseException ex) throws IOException, ParserConfigurationException, SAXException
    {
        File f = new File(mXMLFileName + ".ko");
                    File nuovo = new File("N_" + mXMLFileName);

                    FileWriter fw = new FileWriter(f, true);
                    BufferedWriter bw = new BufferedWriter(fw);
                    
                    if (!f.exists()) {
                        f.createNewFile();
                    }
                   
                    System.out.println(mXMLFileName + " fails to validate because: \n");
                    System.out.println(ex.getMessage());
                    System.out.println("At: " + ex.getLineNumber()
                            + ":" + ex.getColumnNumber());

                    bw.write(mXMLFileName + " fails to validate because: \n");
                    bw.write(ex.getMessage());
                    bw.write("At: " + ex.getLineNumber()
                            + ":" + ex.getColumnNumber());

                    String compromessa = null;
                    int cont = 0;

                    BufferedReader readers = new BufferedReader(new InputStreamReader(new FileInputStream(mXMLFileName), "UTF-8"));

                    int rows = ex.getLineNumber();
                    while (readers.readLine() != null) {
                        if (rows - 2 == cont) {
                            compromessa = readers.readLine();
                        }
                        cont++;
                    }
                    

                    String[] comp = compromessa.split(">");
                    String key = comp[0].replace("<", "");
                    String[] key2arr= comp[1].split("<");
                    String key2=key2arr[1].replace("/", "");
                    key2=key2.replace(">", "");
                    

                    DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
                    Document doc = docBuilder.parse(mXSDFileName);
                    NodeList list = doc.getElementsByTagName("xs:element");
                    int flag = 0;
                    for (int i = 0; i < list.getLength(); i++) {

                        Element first = (Element) list.item(i);
                        if (first.hasAttributes()) {
                            String nm = first.getAttribute("name");
                            if (nm.equalsIgnoreCase(key)||nm.equalsIgnoreCase(key2)) {

                                String nm1 = first.getAttribute("minOccurs");
                                if (nm1.equalsIgnoreCase("1")) {
                                    System.out.println("impossibile correggere automaticamente il file, correggere manualmente il file secondo le indicazioni di log.");
                                    bw.write("impossibile correggere il file!");
                                    System.exit(VALIDATION_FAIL);
                                } else {

                                    flag++;
                                    bw.write(System.getProperty("line.separator")+System.getProperty("line.separator")+"file corretto correttamente, i tag corrotti sono stati rimossi.(tag:<"+key+"></"+key2+">)");
                                }
                            }
                        }

                    }
                    if (flag != 0) {
                        int riga = 0;
                        System.out.println(flag);
                        String fl;
                        if (!nuovo.exists()) {
                            nuovo.createNewFile();
                        }
                        FileWriter n = new FileWriter(nuovo);
                        BufferedWriter bwr = new BufferedWriter(n);
                        readers = new BufferedReader(new InputStreamReader(new FileInputStream(mXMLFileName), "UTF-8"));
                        for (int i = 0; i < cont + flag; i++) {
                            try {

                                fl = readers.readLine();
                                if (fl.contains(compromessa)) {
                                    System.out.println("1 tag corrotto rilevato ed eliminato, visionare il file:"+f.getName());
                                } else {

                                    
                                    bwr.write(fl + System.getProperty("line.separator"));
                                    riga++;

                                }

                            } catch (Exception e) {

                                break;
                            }
                        }
                        bwr.close();
                        n.close();

                    }
                    readers = new BufferedReader(new InputStreamReader(new FileInputStream(nuovo), "UTF-8"));
                    FileWriter write=new FileWriter(mXMLFileName);
                    BufferedWriter writers=new BufferedWriter(write);
                    String app;
                    do{
                       app= readers.readLine();
                       
                        if(app!=null)
                            writers.write(app + System.getProperty("line.separator"));
                    }while(app!=null);
                    readers.close();
                    writers.close();
                    write.close();
                    nuovo.delete();
                    bw.close();
                    fw.close();
                    
                   
                    
                    
    }
    private static URL testURL(final String url) throws MalformedURLException {
        try {
            return new URL(url);
        } catch (final MalformedURLException e) {
        }
        return new URL("file:" + url);
    }

    private static void parseArgs(final String[] args) {

        int argNo = 0;
        String currentArg;
        char flag;

        while (argNo < args.length && args[argNo].startsWith("-")) {
            currentArg = args[argNo++];

            if (currentArg.equals("--version")) {
                printVersionAndExit();
            } else if (currentArg.equals("--help")) {
                printHelpAndExit();
            } else {

                for (int charNo = 1; charNo < currentArg.length(); charNo++) {
                    flag = currentArg.charAt(charNo);
                    switch (flag) {
                        case 'V':
                            printVersionAndExit();
                            break;
                        case 'h':
                            printHelpAndExit();
                            break;

                        default:
                            System.err.println("Illegal option " + flag);
                            printUsageAndExit();
                            break;
                    }
                }
            }
        }

        if ((argNo + 2) != args.length) {

            printUsageAndExit();
        }

        mXSDFileName = args[argNo];
        mXMLFileName = args[++argNo];
    }

    public static void printUsageAndExit() {

        System.err.println(
                "Usage: " + PROGRAM_NAME + " [OPTION]... XSDFILE XMLFILE");
        System.exit(2); //2 indicates incorrect usage
    }

    public static void printHelpAndExit() {

        System.out.print(
                "\nUsage: " + PROGRAM_NAME + " [OPTION]... XSDFILE XMLFILE\n\n "
                + "Validates the XML document at XMLFILE against the XML Schema at"
                + " XSDFILE.\n\n"
                + "--version  -V  Output version number.\n"
                + "--help  -h  Output this help.\n");

        System.exit(0);
    }

    public static void printVersionAndExit() {

        System.out.println(PROGRAM_NAME + " Version " + VERSION + "\n");
        System.exit(0);
    }

}
